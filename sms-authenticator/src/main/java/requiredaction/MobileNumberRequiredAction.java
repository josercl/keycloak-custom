package requiredaction;

import authenticator.factory.SmsAuthenticatorFactory;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.InitiatedActionSupport;
import org.keycloak.authentication.RequiredActionContext;
import org.keycloak.authentication.RequiredActionProvider;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.UserModel;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.provider.ProviderFactory;
import org.keycloak.services.validation.Validation;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.Optional;
import java.util.function.Consumer;

public class MobileNumberRequiredAction implements RequiredActionProvider {

    public static final String PROVIDER_ID = "mobile-number-ra";
    public static final String MOBILE_NUMBER_FIELD = "mobile_number";
    public static final String TPL_CODE = "update-mobile-number.ftl";

    @Override
    public InitiatedActionSupport initiatedActionSupport() {
        return InitiatedActionSupport.SUPPORTED;
    }

    @Override
    public void evaluateTriggers(RequiredActionContext context) {
        String mobileNumberAttribute = context.getUser().getFirstAttribute(MOBILE_NUMBER_FIELD);
        if (Validation.isBlank(mobileNumberAttribute)) {
            context.getUser().addRequiredAction(PROVIDER_ID);
        }
    }

    @Override
    public void requiredActionChallenge(RequiredActionContext context) {
        context.challenge(createForm(context, null));
    }

    @Override
    public void processAction(RequiredActionContext context) {
        UserModel user = context.getUser();

        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
        String mobileNumber = formData.getFirst(MOBILE_NUMBER_FIELD);

        if (Validation.isBlank(mobileNumber)) {
            context.challenge(createForm(
                    context,
                    form -> form.addError(new FormMessage(MOBILE_NUMBER_FIELD, "Invalid number"))
            ));
        }

        user.setSingleAttribute(MOBILE_NUMBER_FIELD, mobileNumber);

        context.success();
    }

    @Override
    public void close() {

    }

    private Response createForm(RequiredActionContext context, Consumer<LoginFormsProvider> formConsumer) {
        LoginFormsProvider form = context.form();
        form.setAttribute("username", context.getUser().getUsername());

        String mobileNumber = context.getUser().getFirstAttribute(MOBILE_NUMBER_FIELD);
        form.setAttribute(MOBILE_NUMBER_FIELD, Optional.ofNullable(mobileNumber).orElse(""));

        Optional.ofNullable(formConsumer).ifPresent(f -> f.accept(form));

        return form.createForm(TPL_CODE);
    }
}
