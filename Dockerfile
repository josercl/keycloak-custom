FROM quay.io/keycloak/keycloak:21.0

WORKDIR /opt/keycloak

COPY ./sms-authenticator/target/sms-authenticator.jar /opt/keycloak/providers/
COPY ./theme/target/theme.jar /opt/keycloak/providers/

EXPOSE 8080

ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start-dev"]